﻿namespace Ana_Lucas
{
    public class Profesor : Empleado
    {
        private string departamento;

        public Profesor(string departamento, int anioIncorporacion, string numeroDespacho, string nombre, string apellidos, string estadoCivil, string identificacion) : base(anioIncorporacion, numeroDespacho ,nombre, apellidos, estadoCivil, identificacion)
        {
            this.departamento = departamento;
        }

        public void cambiarDepartamento(string departamento)
        {
            this.departamento = departamento;
        }

        public void imprimir()
        {
            base.imprimir();    
            Console.WriteLine("Departamento: " + this.departamento + "\n");
        }
    }
}
