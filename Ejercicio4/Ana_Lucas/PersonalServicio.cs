﻿namespace Ana_Lucas
{
    public class PersonalServicio : Empleado
    {
        private string seccion;

        public PersonalServicio(string seccion, int anioIncorporacion, string numeroDespacho, string nombre, string apellidos, string estadoCivil, string identificacion) : base(anioIncorporacion, numeroDespacho, nombre, apellidos, estadoCivil, identificacion)
        {
            this.seccion = seccion;
        }

        public void trasladoPersonal(string seccion)
        {
            this.seccion = seccion;
        }

        public void imprimir()
        {
            base.imprimir();
            Console.WriteLine("Seccion: " + this.seccion + "\n");
        }
    }

}
