﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ana_Lucas
{
    public class Persona
    {
        private string nombre, apellidos, estadoCivil, identificacion;

        public Persona(string nombre, string apellidos, string estadoCivil, string identificacion)
        {
            this.nombre = nombre;   
            this.apellidos = apellidos; 
            this.estadoCivil = estadoCivil;
            this.identificacion = identificacion;
        }

        public void cambiarEstadoCivil(string estadoCivil)
        {
            this.estadoCivil = estadoCivil;
        }

        protected void imprimir()
        {
            Console.WriteLine("Nombre: " + this.nombre);
            Console.WriteLine("Apellidos: " + this.apellidos);
            Console.WriteLine("Estado Civil: " + this.estadoCivil);
            Console.WriteLine("No. Idetificacion: " + this.identificacion);
        }
    }
}
