﻿namespace Ana_Lucas
{
        public class Estudiante : Persona
    {
        private string curso;

        public Estudiante(string curso, string nombre, string apellidos, string estadoCivil, string identificacion)  : base(nombre, apellidos, estadoCivil, identificacion) 
        {
            this.curso = curso;
        }

        public void imprimir()
        {
            base.imprimir();
            Console.WriteLine("Curso: " + this.curso + "\n");
        }

        public void matricular(string curso)
        {
            this.curso = curso;
        }

    }
}
