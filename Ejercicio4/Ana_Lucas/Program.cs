﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ana_Lucas
{
    public class Program
    {
        
        static void Main(String []args)
        {
            Profesor profesor = new Profesor("Desarrollo de Software", 2000, "113405", "Ana", "Lucas", "Soltera", "87654302");
            PersonalServicio personalServicio = new PersonalServicio("Biblioteca", 2021, "76662", "Luis", "Perez", "Casado", "98765623");
            Estudiante estudiante = new Estudiante("Estructura de Datos", "Jose", "Hernandez", "Soltero", "39333716");

            personalServicio.cambiarEstadoCivil("Soltero");
            profesor.cambiarDespacho("99999999");
            estudiante.matricular("Ingenieria de Sortware III");
            profesor.cambiarDespacho("88");
            personalServicio.trasladoPersonal("90");

            profesor.imprimir();
            personalServicio.imprimir();
            estudiante.imprimir();
        }

    }
}
