﻿namespace Ana_Lucas
{
    public class Empleado : Persona
    {
        private int anioIncorporacion;
        private string numeroDespacho;

        public Empleado(int anioIncorporacion, string numeroDespacho, string nombre, string apellidos, string estadoCivil, string identificacion) : base(nombre, apellidos, estadoCivil, identificacion)
        {
            this.anioIncorporacion = anioIncorporacion;
            this.numeroDespacho = numeroDespacho;
        }

        protected void imprimir()
        {
            base.imprimir();
            Console.WriteLine("Fecha Incorporacion: " + this.anioIncorporacion);
            Console.WriteLine("Numero de Despacho: " + this.numeroDespacho);
        }

        public void cambiarDespacho(string numeroDespacho)
        {
            this.numeroDespacho = numeroDespacho;
        }

    }
}
